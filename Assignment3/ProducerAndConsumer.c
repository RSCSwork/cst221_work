#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<unistd.h>

/*
Rikk Shimizu, this is my code.

Code Synopsis: We define a buffer_size of 8 to match the size of a WORD.
In the main two threads are created, one for Producer, and one for Consumer.
When the threads start, the producer is started first, will go through a cycle 
and then put itself to sleep, the consumer will then start and go through its
own cycle and put itself to sleep. They should be alternating between actions,
but neither should ever have to wait. The sleep timers have a small delay so
the functions can run alternating.  
*/

int put();
int get();

//buffer_size is based on the size of a WORD
#define buffer_size 8
static int mybuffer[buffer_size];
int theProduct = 0;
int counter = 0;
int counterGet = 0;

//Producer function will call put, and increment theProduct.
void *producer(){
	int i;
	while(1){
		put(theProduct++);
		//sleep thread for producer
		sleep(3);
	}
}//ends producer

//Consumer function will call get
void *consumer(){
	int i;
	while(1){	
		i = get();
		//sleep thread for consumer
		sleep(4);
	}
}//ends consumer

//Put function will take in a number and place that number into
//mybuffer, "producing" number. It will also check to make sure 
//that we are within the WORD size, and if we exceed the WORD size 
//the counter is reset to keep us in bounds and overwrite any previous
//data. Produce prints the numbers it creates and adds into the buffer
int put(int i){

	if(counter < buffer_size){
		mybuffer[counter] = i;
		printf("Pro %i \n", i);
		counter++;
		if(counter == buffer_size){
				counter = 0;
			}
		return 1;
	}else{
		return -1;
	}
}//ends put

//Get function will grab a number from mybuffer, and return it to consumer.
//In this function we follow produce and grab numbers it has put into 
//the mybuffer. We print out the numbers that consumer grabs here.
int get(){
	if(counterGet < buffer_size){
		int grabbed = mybuffer[counterGet];
		if(grabbed == -1){
			return -1;
		}else{
			mybuffer[counterGet] = -1;
			printf("Con %i \n", grabbed);
			counterGet++;
			if(counterGet == buffer_size){
				counterGet = 0;
			}
			return grabbed;
		}
	}else{
		return -1;
	}
	
}//ends get


void main(int argc, char *argv[]){

	pthread_t threadPro, threadCon;
	
	for(int i = 0; i<buffer_size; i++){
		mybuffer[i] = -1;
	}//ends for loop, initialize buffer to -1
	
	//Creating our 2 threads, one for each process.
	pthread_create(&threadPro, NULL, producer, NULL);
	pthread_create(&threadCon, NULL, consumer, NULL);
	
	pthread_exit(NULL);
	
}//ends main
