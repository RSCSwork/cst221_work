Hello,

My name is Rikk Shimizu, I am currently working on my second Bachelor's degree in Computer Science.
I work full time for a Storage Company, which is why I chose to get this second degree online. In my 
free time I shoot archery, and compete in flat target and 3D. I enjoy reading, and learning various skills. 
I also try to get out and camp or backpack in my free time.

In terms of programming, I have some experience in C, and at work have worked in a Linux environment. Although
my Linux experience has been limited to the usage for my teams work. There is still alot about Linux I would
like to learn, in order to be more efficient. 

-Rikk